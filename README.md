A set of bootstrap scripts to spin up a single host Rancher instance with persistent storage.

To tailor to your environment, replace the following strings with your own values in all the files:

    {{RANCHER_AGENT_REGISTRATION_URL}}
    {{DOMAIN}}


Take a look at `runbook.txt` for quick setup commands

The included `nginx/*` structure goes in `/mnt/data/rancher/nginx/`
